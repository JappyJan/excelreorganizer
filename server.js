const express = require("express");
const app = express();
const history = require("connect-history-api-fallback");
const path = require("path");
const compression = require('compression');

app.use(history({
    verbose: true
}));

app.use(compression());

app.use(express.static(path.join(__dirname, 'dist')));

app.set("port", (process.env.PORT || 80));

app.listen(app.get("port"), () => {
    console.log(`ENV: ${process.env.NODE_ENV}`);
    console.log(`app is running at localhost: ${app.get('port')}`);
});
